# MoinMoin - plantuml parser

PURPOSE : To embed plantuml diagrams inside moinmoin

REQUISITES : Install the plantweb library.

INPUTS : This is a processor, You can embed a plantuml diagram with
the plantuml syntax. Refer to http://plantuml.com/activity-diagram-legacy

OUTPUT : An image of the plantuml representation

EXAMPLES::

As usual, you put the name of the parser and you use it in a normal
moinmoin page

```
{{{#!plantumi
(*) --> "Escribe algo"
If "¿Buena ortografía?" then
--> [¡Sí!] (*)
else
--> [No :(] "Haz planas"
Endif
--> "Escribe algo"
}}}
```

Would produce something like

![Sample plantuml Image](/assets/simpleaccentsample.png)

Caution: Make sure you use plantumi, with an i at the end, not an l

