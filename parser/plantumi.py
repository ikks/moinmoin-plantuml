# -*- coding: utf-8 -*-
"""
    MoinMoin - PlantUML a simple consumer of
    a plantuml server
"""

from posixpath import join
from plantweb.plantuml import compress_and_encode


Dependencies = []
SERVER_NAME = u'https://www.plantuml.com/plantuml'
IMG_FORMAT = u'png'


class Parser:

    def __init__(self, raw, request, **kw):
        self.raw = raw
        self.request = request
        self.style = None

    def format(self, formatter):

        request = self.request
        raw = self.raw
        try:
            encoded = compress_and_encode(raw)
            img_link = join(SERVER_NAME, IMG_FORMAT, encoded)
        except ImportError:
            self.request.write('<p>Make sure you have plantweb installed.')
            return
        style = self.style
        if style is None:
            style = 'default'
        self.request.write(formatter.rawHTML(
            '''<img src="%s" alt="">''' % img_link
        ))
